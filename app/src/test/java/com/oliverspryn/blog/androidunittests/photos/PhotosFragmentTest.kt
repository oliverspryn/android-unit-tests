package com.oliverspryn.blog.androidunittests.photos

import android.view.View
import android.view.ViewGroup
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.dagger.components.MainComponent
import com.winterbe.expekt.expect
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class PhotosFragmentTest : Spek({

    describe("The PhotosFragment") {

        var mockMainComponent: MainComponent? = null
        var uut: PhotosFragment? = null

        beforeEachTest {
            mockMainComponent = mock()

            uut = PhotosFragment(
                mainComponent = mockMainComponent
            )

            uut?.linearLayoutManagerFactory = mock()
            uut?.photosController = mock()
            uut?.viewMvcFactory = mock()
        }

        describe("when init") {

            describe("when there is a MainComponent in the Dagger object graph") {

                it("injects the necessary dependencies") {
                    verify(mockMainComponent)?.inject(any<PhotosFragment>())
                }
            }

            describe("when there isn't a MainComponent in the Dagger object graph") {

                beforeEachTest {
                    mockMainComponent = mock()

                    uut = PhotosFragment(
                        mainComponent = null
                    )
                }

                it("does not inject the necessary dependencies") {
                    verify(mockMainComponent, never())?.inject(any<PhotosFragment>())
                }
            }
        }

        describe("when onCreateView") {

            var mockPhotosViewMvc: PhotosViewMvc? = null
            var mockRootView: View? = null
            var result: View? = null

            beforeEachTest {

                val mockContainer: ViewGroup = mock()
                mockPhotosViewMvc = mock()
                mockRootView = mock()

                whenever(mockPhotosViewMvc?.rootView).thenReturn(mockRootView)
                whenever(uut?.viewMvcFactory?.getPhotosViewMvc(uut?.linearLayoutManagerFactory!!, mockContainer)).thenReturn(mockPhotosViewMvc)

                result = uut?.onCreateView(
                    inflater = mock(),
                    container = mockContainer,
                    savedInstanceState = mock()
                )
            }

            it("sets the view MVC on the controller") {
                verify(uut?.photosController)?.viewMvc = mockPhotosViewMvc
            }

            it("returns the root view from that view MVC") {
                expect(result).to.equal(mockRootView)
            }
        }

        describe("when onStart") {

            beforeEachTest {
                uut?.onStart()
            }

            it("calls onStart on the controller") {
                verify(uut?.photosController)?.onStart()
            }
        }

        describe("when onDestroy") {

            beforeEachTest {
                uut?.onDestroy()
            }

            it("calls onDestroy on the controller") {
                verify(uut?.photosController)?.onDestroy()
            }
        }
    }
})
