package com.oliverspryn.blog.androidunittests.photos

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doNothing
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.R
import com.oliverspryn.blog.androidunittests.mvc.ViewMvcFactory
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavDirectionsForwarder
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavigationForwarder
import com.oliverspryn.blog.androidunittests.wrappers.PhotosFactory
import com.oliverspryn.blog.androidunittests.wrappers.PicassoForwarder
import com.oliverspryn.blog.androidunittests.wrappers.SchedulerForwarder
import com.oliverspryn.blog.androidunittests.wrappers.android.ToastForwarder
import com.winterbe.expekt.expect
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class PhotosControllerTest : Spek({

    describe("The PhotosController") {

        var mockCompositeDisposable: CompositeDisposable? = null
        var mockNavDirectionsForwarder: NavDirectionsForwarder? = null
        var mockNavigationForwarder: NavigationForwarder? = null
        var mockPhotosFactory: PhotosFactory? = null
        var mockPhotosApiService: PhotosApiService? = null
        var mockPicassoForwarder: PicassoForwarder? = null
        var mockSchedulerForwarder: SchedulerForwarder? = null
        var mockToastForwarder: ToastForwarder? = null
        var mockViewMvcFactory: ViewMvcFactory? = null

        var uut: PhotosController? = null

        beforeEachTest {

            mockCompositeDisposable = mock()
            mockNavDirectionsForwarder = mock()
            mockNavigationForwarder = mock()
            mockPhotosFactory = mock()
            mockPhotosApiService = mock()
            mockPicassoForwarder = mock()
            mockSchedulerForwarder = mock()
            mockToastForwarder = mock()
            mockViewMvcFactory = mock()

            uut = spy(PhotosController(
                compositeDisposable = mockCompositeDisposable!!,
                navDirectionsForwarder = mockNavDirectionsForwarder!!,
                navigationForwarder = mockNavigationForwarder!!,
                photosFactory = mockPhotosFactory!!,
                photosApiService = mockPhotosApiService!!,
                picassoForwarder = mockPicassoForwarder!!,
                schedulerForwarder = mockSchedulerForwarder!!,
                toastForwarder = mockToastForwarder!!,
                viewMvcFactory = mockViewMvcFactory!!
            ))

            uut?.viewMvc = mock()
        }

        describe("when onStart") {

            var mockAdapter: PhotosAdapter? = null

            beforeEachTest {

                mockAdapter = mock()

                whenever(mockPhotosFactory?.newAdapterInstance(
                    listener = uut!!,
                    picassoForwarder = mockPicassoForwarder!!,
                    viewMvcFactory = mockViewMvcFactory!!
                )).thenReturn(mockAdapter)

                doNothing().whenever(uut)?.getPhotos()
            }

            describe("and the view MVC instance is null") {

                beforeEachTest {
                    uut?.viewMvc = null
                    uut?.onStart()
                }

                it("does not interact with the view MVC instance") {
                    verify(uut, never())?.viewMvc
                }

                it("fetches the photos from the API") {
                    verify(uut)?.getPhotos()
                }

                it("caches the adapter for later use") {
                    expect(uut?.photosAdapter).to.equal(mockAdapter)
                }
            }

            describe("and the view MVC instance is not null") {

                beforeEachTest {
                    uut?.onStart()
                }

                it("sets the adapter on the view MVC instance") {
                    verify(uut?.viewMvc)?.setAdapter(mockAdapter!!)
                }

                it("fetches the photos from the API") {
                    verify(uut)?.getPhotos()
                }

                it("caches the adapter for later use") {
                    expect(uut?.photosAdapter).to.equal(mockAdapter)
                }
            }
        }

        describe("when onDestroy") {

            beforeEachTest {
                uut?.onDestroy()
            }

            it("clears the composite disposable") {
                verify(mockCompositeDisposable)?.clear()
            }
        }

        describe("when onPhotoTap") {

            var mockNavController: NavController? = null
            var mockNavDirections: NavDirections? = null
            var mockModel: PhotosModel? = null

            beforeEachTest {

                mockModel = PhotosModel(
                    id = 42,
                    thumbnailUrl = "https://ddg.co/",
                    title = "DuckDuckGo",
                    url = "https://duckduckgo.com/"
                )

                mockNavController = mock()
                mockNavDirections = mock()

                val mockRootView: View = mock()

                whenever(mockNavDirectionsForwarder?.actionPhotoListFragmentToDetailsFragment(mockModel!!)).thenReturn(mockNavDirections)
                whenever(mockNavigationForwarder?.findNavController(mockRootView)).thenReturn(mockNavController)
                whenever(uut?.viewMvc?.rootView).thenReturn(mockRootView)
            }

            describe("and the view MVC instance is null") {

                beforeEachTest {
                    uut?.viewMvc = null
                    uut?.onPhotoTap(mockModel!!)
                }

                it("does not navigate to the details screen") {
                    verify(mockNavController, never())?.navigate(any<NavDirections>())
                }
            }

            describe("and the root view on the view MVC instance is null") {

                beforeEachTest {
                    whenever(uut?.viewMvc?.rootView).thenReturn(null)
                    uut?.onPhotoTap(mockModel!!)
                }

                it("does not navigate to the details screen") {
                    verify(mockNavController, never())?.navigate(any<NavDirections>())
                }
            }

            describe("and neither the view MVC instance nor the root view are null") {

                beforeEachTest {
                    uut?.onPhotoTap(mockModel!!)
                }

                it("navigates to the details screen with the provided data model") {
                    verify(mockNavController)?.navigate(mockNavDirections!!)
                }
            }
        }

        describe("when getPhotos") {

            var ioTestScheduler: TestScheduler? = null
            var mainThreadTestScheduler: TestScheduler? = null

            beforeEachTest {
                ioTestScheduler = TestScheduler()
                mainThreadTestScheduler = TestScheduler()

                whenever(mockSchedulerForwarder?.io).thenReturn(ioTestScheduler)
                whenever(mockSchedulerForwarder?.mainThread).thenReturn(mainThreadTestScheduler)
            }

            describe("and the call succeeds") {

                var data: List<PhotosModel>? = null

                beforeEachTest {

                    val photo1 = PhotosModel(
                        id = 42,
                        thumbnailUrl = "https://ddg.co/",
                        title = "DuckDuckGo",
                        url = "https://duckduckgo.com/"
                    )

                    val photo2 = PhotosModel(
                        id = 42,
                        thumbnailUrl = "https://ddg.co/",
                        title = "DuckDuckGo",
                        url = "https://duckduckgo.com/"
                    )

                    data = listOf(photo1, photo2)
                    whenever(mockPhotosApiService?.getPhotos()).thenReturn(Single.just(data))
                }

                describe("and the photos adapter is null") {

                    beforeEachTest {
                        uut?.photosAdapter = null
                        uut?.getPhotos()

                        ioTestScheduler?.triggerActions()
                        mainThreadTestScheduler?.triggerActions()
                    }

                    it("adds a disposable to the composite disposable") {
                        verify(mockCompositeDisposable)?.add(any())
                    }
                }

                describe("and the photos adapter is not null") {

                    beforeEachTest {
                        uut?.photosAdapter = mock()
                        uut?.getPhotos()

                        ioTestScheduler?.triggerActions()
                        mainThreadTestScheduler?.triggerActions()
                    }

                    it("updates the photos adapter the with fetched data") {
                        verify(uut?.photosAdapter)?.updatePhotos(data!!)
                    }

                    it("adds a disposable to the composite disposable") {
                        verify(mockCompositeDisposable)?.add(any())
                    }
                }
            }

            describe("and the call does not succeed") {

                var mockContext: Context? = null
                var mockRootView: View? = null
                var mockToast: Toast? = null

                beforeEachTest {
                    mockContext = mock()
                    mockRootView = mock()
                    mockToast = mock()

                    whenever(mockContext?.getString(R.string.network_error)).thenReturn("Error")
                    whenever(mockPhotosApiService?.getPhotos()).thenReturn(Single.error(Throwable()))
                    whenever(mockRootView?.context).thenReturn(mockContext)
                    whenever(mockToastForwarder?.makeText(mockContext!!, "Error", Toast.LENGTH_LONG)).thenReturn(mockToast)
                    whenever(uut?.viewMvc?.rootView).thenReturn(mockRootView)
                }

                describe("and the view MVC instance is null") {

                    beforeEachTest {
                        uut?.viewMvc = null
                        uut?.getPhotos()

                        ioTestScheduler?.triggerActions()
                        mainThreadTestScheduler?.triggerActions()
                    }

                    it("does not create or show any toast messages") {
                        verify(mockToastForwarder, never())?.makeText(
                            context = any(),
                            text = any(),
                            duration = any()
                        )

                        verify(mockToast, never())?.show()
                    }

                    it("adds a disposable to the composite disposable") {
                        verify(mockCompositeDisposable)?.add(any())
                    }
                }

                describe("and the root view on the view MVC instance is null") {

                    beforeEachTest {
                        whenever(uut?.viewMvc?.rootView).thenReturn(null)
                        uut?.getPhotos()

                        ioTestScheduler?.triggerActions()
                        mainThreadTestScheduler?.triggerActions()
                    }

                    it("does not create or show any toast messages") {
                        verify(mockToastForwarder, never())?.makeText(
                            context = any(),
                            text = any(),
                            duration = any()
                        )

                        verify(mockToast, never())?.show()
                    }

                    it("adds a disposable to the composite disposable") {
                        verify(mockCompositeDisposable)?.add(any())
                    }
                }

                describe("and the context on the root view on the view MVC instance is null") {

                    beforeEachTest {
                        whenever(mockRootView?.context).thenReturn(null)
                        uut?.getPhotos()

                        ioTestScheduler?.triggerActions()
                        mainThreadTestScheduler?.triggerActions()
                    }

                    it("does not create or show any toast messages") {
                        verify(mockToastForwarder, never())?.makeText(
                            context = any(),
                            text = any(),
                            duration = any()
                        )

                        verify(mockToast, never())?.show()
                    }

                    it("adds a disposable to the composite disposable") {
                        verify(mockCompositeDisposable)?.add(any())
                    }
                }

                describe("and neither context nor the view MVC instance nor the root view are null") {

                    describe("but the toast creation process fails") {
                        beforeEachTest {
                            whenever(mockToastForwarder?.makeText(mockContext!!, "Error", Toast.LENGTH_LONG)).thenReturn(null)
                            uut?.getPhotos()

                            ioTestScheduler?.triggerActions()
                            mainThreadTestScheduler?.triggerActions()
                        }

                        it("creates a toast message, but does not show it") {
                            verify(mockToastForwarder)?.makeText(
                                context = mockContext!!,
                                text = "Error",
                                duration = Toast.LENGTH_LONG
                            )

                            verify(mockToast, never())?.show()
                        }

                        it("adds a disposable to the composite disposable") {
                            verify(mockCompositeDisposable)?.add(any())
                        }
                    }

                    describe("but the toast creation process succeeds") {

                        beforeEachTest {
                            uut?.getPhotos()

                            ioTestScheduler?.triggerActions()
                            mainThreadTestScheduler?.triggerActions()
                        }

                        it("creates and shows a toast message with an error message") {
                            verify(mockToastForwarder)?.makeText(
                                context = mockContext!!,
                                text = "Error",
                                duration = Toast.LENGTH_LONG
                            )

                            verify(mockToast)?.show()
                        }

                        it("adds a disposable to the composite disposable") {
                            verify(mockCompositeDisposable)?.add(any())
                        }
                    }
                }
            }
        }
    }
})
