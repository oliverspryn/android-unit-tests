package com.oliverspryn.blog.androidunittests.details

import android.net.Uri
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.photos.PhotosModel
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavDirectionsForwarder
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavigationForwarder
import com.oliverspryn.blog.androidunittests.wrappers.android.UriForwarder
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class DetailsControllerTest : Spek({

    describe("The DetailsController") {

        var mockNavDirectionsForwarder: NavDirectionsForwarder? = null
        var mockNavigationForwarder: NavigationForwarder? = null
        var mockUriForwarder: UriForwarder? = null

        var uut: DetailsController? = null

        beforeEachTest {
            mockNavDirectionsForwarder = mock()
            mockNavigationForwarder = mock()
            mockUriForwarder = mock()

            uut = spy(DetailsController(
                navDirectionsForwarder = mockNavDirectionsForwarder!!,
                navigationForwarder = mockNavigationForwarder!!,
                uriForwarder = mockUriForwarder!!
            ))
        }

        describe("when onCreateView") {

            var model: PhotosModel? = null

            beforeEachTest {
                model = PhotosModel(
                    id = 42,
                    thumbnailUrl = "https://ddg.co/",
                    title = "DuckDuckGo",
                    url = "https://duckduckgo.com/"
                )
            }

            describe("and the view MVC instance is null") {

                beforeEachTest {
                    uut?.viewMvc = null
                    uut?.onCreateView(model!!)
                }

                it("does not use the view MVC instance") {
                    verify(uut, never())?.viewMvc
                }
            }

            describe("and the view MVC instance is not null") {

                beforeEachTest {
                    uut?.viewMvc = mock()
                    uut?.onCreateView(model!!)
                }

                it("sets the photo on the view MVC instance") {
                    verify(uut?.viewMvc)?.setPhoto("https://duckduckgo.com/")
                }

                it("sets the title on the view MVC instance") {
                    verify(uut?.viewMvc)?.setTitle("DuckDuckGo")
                }
            }
        }

        describe("when onStart") {

            describe("and the view MVC instance is null") {

                beforeEachTest {
                    uut?.viewMvc = null
                    uut?.onStart()
                }

                it("does not use the view MVC instance") {
                    verify(uut, never())?.viewMvc
                }
            }

            describe("and the view MVC instance is not null") {

                beforeEachTest {
                    uut?.viewMvc = mock()
                    uut?.onStart()
                }

                it("registers the provided listener on the view MVC instance") {
                    verify(uut?.viewMvc)?.registerListener(uut!!)
                }
            }
        }

        describe("when onDestroy") {

            describe("and the view MVC instance is null") {

                beforeEachTest {
                    uut?.viewMvc = null
                    uut?.onDestroy()
                }

                it("does not use the view MVC instance") {
                    verify(uut, never())?.viewMvc
                }
            }

            describe("and the view MVC instance is not null") {

                beforeEachTest {
                    uut?.viewMvc = mock()
                    uut?.onDestroy()
                }

                it("unregisters the provided listener from the view MVC instance") {
                    verify(uut?.viewMvc)?.unregisterListener(uut!!)
                }
            }
        }

        describe("when onPreviewTap") {

            var mockNavController: NavController? = null
            var mockNavDirections: NavDirections? = null
            var mockRootView: View? = null

            beforeEachTest {
                mockNavController = mock()
                mockNavDirections = mock()
                mockRootView = mock()

                val mockUri: Uri = mock()

                whenever(mockNavDirectionsForwarder?.actionDetailsFragmentToPreviewPictureUrl(mockUri)).thenReturn(mockNavDirections)
                whenever(mockNavigationForwarder?.findNavController(mockRootView!!)).thenReturn(mockNavController)
                whenever(mockUriForwarder?.parse("https://ddg.co/")).thenReturn(mockUri)
            }

            describe("and the view MVC instance is null") {

                beforeEachTest {
                    uut?.viewMvc = null
                    uut?.onPreviewTap("https://ddg.co/")
                }

                it("does not navigate to the given URL") {
                    verify(mockNavController, never())?.navigate(any<NavDirections>())
                }
            }

            describe("and the root view on the view MVC instance is null") {

                beforeEachTest {
                    uut?.viewMvc = mock()
                    whenever(uut?.viewMvc?.rootView).thenReturn(null)

                    uut?.onPreviewTap("https://ddg.co/")
                }

                it("does not navigate to the given URL") {
                    verify(mockNavController, never())?.navigate(any<NavDirections>())
                }
            }

            describe("and both the root view and the the view MVC instance are not null") {

                beforeEachTest {
                    uut?.viewMvc = mock()
                    whenever(uut?.viewMvc?.rootView).thenReturn(mockRootView)

                    uut?.onPreviewTap("https://ddg.co/")
                }

                it("navigates to the given URL") {
                    verify(mockNavController)?.navigate(mockNavDirections!!)
                }
            }
        }
    }
})
