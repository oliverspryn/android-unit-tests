package com.oliverspryn.blog.androidunittests.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.button.MaterialButton
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.R
import com.oliverspryn.blog.androidunittests.wrappers.PicassoForwarder
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import com.winterbe.expekt.expect
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class DetailsViewMvcImplTest : Spek({

    describe("The DetailsViewMvcImpl") {

        var mockButton: MaterialButton? = null
        var mockPhoto: AppCompatImageView? = null
        var mockPicassoForwarder: PicassoForwarder? = null
        var mockRootView: View? = null
        var mockTitle: AppCompatTextView? = null

        var uut: DetailsViewMvcImpl? = null

        beforeEachTest {

            mockButton = mock()
            mockPhoto = mock()
            mockPicassoForwarder = mock()
            mockTitle = mock()

            mockRootView = mock {
                on { findViewById<MaterialButton>(R.id.view_online_button) } doReturn mockButton
                on { findViewById<AppCompatImageView>(R.id.photo_preview) } doReturn mockPhoto
                on { findViewById<AppCompatTextView>(R.id.full_title) } doReturn mockTitle
            }

            val mockParent: ViewGroup = mock()
            val mockInflater: LayoutInflater = mock {
                on { inflate(R.layout.details_fragment, mockParent, false) } doReturn mockRootView
            }

            uut = DetailsViewMvcImpl(
                inflater = mockInflater,
                parent = mockParent,
                picassoForwarder = mockPicassoForwarder!!
            )
        }

        describe("when init") {

            it("inflates the layout and sets it as the root view") {
                expect(uut?.rootView).to.equal(mockRootView)
            }

            it("obtains a reference to the button") {
                expect(uut?.button).to.equal(mockButton)
            }

            describe("that row") {
                it("has a click listener") {
                    verify(mockButton)?.setOnClickListener(uut!!)
                }
            }

            it("obtains a reference to the photo") {
                expect(uut?.photo).to.equal(mockPhoto)
            }

            it("obtains a reference to the title") {
                expect(uut?.title).to.equal(mockTitle)
            }
        }

        describe("when setPhoto") {

            var mockPicasso: Picasso? = null
            var mockRequestCreator: RequestCreator? = null

            beforeEachTest {
                mockPicasso = mock()
                mockRequestCreator = mock()

                whenever(mockPicassoForwarder?.get()).thenReturn(mockPicasso)
                whenever(mockPicasso?.load("https://duckduckgo.com/")).thenReturn(mockRequestCreator)
                whenever(mockRequestCreator?.placeholder(android.R.color.darker_gray)).thenReturn(mockRequestCreator)
                whenever(mockRequestCreator?.fit()).thenReturn(mockRequestCreator)

                uut?.setPhoto("https://duckduckgo.com/")
            }

            it("saves a reference to the URI") {
                expect(uut?.cachedUri).to.equal("https://duckduckgo.com/")
            }

            it("loads the requested image into the image view using the fit scaling technique and a gray placeholder") {
                verify(mockPicasso)?.load("https://duckduckgo.com/")
                verify(mockRequestCreator)?.placeholder(android.R.color.darker_gray)
                verify(mockRequestCreator)?.fit()
                verify(mockRequestCreator)?.into(mockPhoto)
            }
        }

        describe("when setTitle") {

            beforeEachTest {
                uut?.setTitle("DuckDuckGo")
            }

            it("sets the title") {
                verify(mockTitle)?.text = "DuckDuckGo"
            }
        }

        describe("when onClick") {

            var listener1: DetailsViewMvc.Listener? = null
            var listener2: DetailsViewMvc.Listener? = null

            beforeEachTest {
                listener1 = spy(object : DetailsViewMvc.Listener {
                    override fun onPreviewTap(uri: String) = Unit
                })

                listener2 = spy(object : DetailsViewMvc.Listener {
                    override fun onPreviewTap(uri: String) = Unit
                })

                uut?.registerListener(listener1!!)
                uut?.registerListener(listener2!!)
            }

            describe("and there is a cached URI") {

                beforeEachTest {
                    uut?.cachedUri = "https://duckduckgo.com/"
                    uut?.onClick(mockButton)
                }

                it("calls onPreviewTap for all registered listeners") {
                    verify(listener1)?.onPreviewTap("https://duckduckgo.com/")
                    verify(listener2)?.onPreviewTap("https://duckduckgo.com/")
                }
            }

            describe("and there is not a cached URI") {

                beforeEachTest {
                    uut?.cachedUri = null
                    uut?.onClick(mockButton)
                }

                it("does not call onPreviewTap for all registered listeners") {
                    verify(listener1, never())?.onPreviewTap(any())
                    verify(listener2, never())?.onPreviewTap(any())
                }
            }

            afterEachTest {
                uut?.unregisterListener(listener1!!)
                uut?.unregisterListener(listener2!!)
            }
        }
    }
})
