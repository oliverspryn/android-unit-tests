package com.oliverspryn.blog.androidunittests.navigation

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.NavigatorProvider
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.dagger.components.MainComponent
import com.oliverspryn.blog.androidunittests.navigation.navigators.HttpNavigator
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class CustomNavHostFragmentTest : Spek({

    describe("The CustomNavHostFragment") {

        var mockMainComponent: MainComponent? = null
        var uut: CustomNavHostFragment? = null

        beforeEachTest {
            mockMainComponent = mock()

            uut = spy(CustomNavHostFragment(
                mainComponent = mockMainComponent
            ))

            uut?.alertDialogBuilderFactory = mock()
            uut?.intentFactory = mock()
            uut?.navigatorFactory = mock()
            uut?.uriForwarder = mock()
        }

        describe("when init") {

            describe("when there is a MainComponent in the Dagger object graph") {

                it("injects the necessary dependencies") {
                    verify(mockMainComponent)?.inject(any<CustomNavHostFragment>())
                }
            }

            describe("when there isn't a MainComponent in the Dagger object graph") {

                beforeEachTest {
                    mockMainComponent = mock()

                    uut = CustomNavHostFragment(
                        mainComponent = null
                    )
                }

                it("does not inject the necessary dependencies") {
                    verify(mockMainComponent, never())?.inject(any<CustomNavHostFragment>())
                }
            }
        }

        describe("when onCreateNavController") {

            var mockHttpNavigator: HttpNavigator? = null
            var mockNavigatorProvider: NavigatorProvider? = null

            beforeEachTest {
                mockHttpNavigator = mock()
                whenever(uut?.navigatorFactory?.newHttpNavigatorInstance(any(), any(), any(), any())).thenReturn(mockHttpNavigator!!)
            }

            describe("and the host fragment has a reference to the containing activity") {

                beforeEachTest {
                    mockNavigatorProvider = mock()

                    val mockContext: Context = mock()
                    val mockFragmentManger: FragmentManager = mock()
                    val mockNavController: NavController = mock {
                        on { navigatorProvider } doReturn mockNavigatorProvider!!
                    }

                    doReturn(mockContext).whenever(uut)?.context
                    doReturn(mockFragmentManger).whenever(uut)?.childFragmentManager
                    whenever(uut?.activity).thenReturn(mock())

                    val onCreateNavController = CustomNavHostFragment::class.java
                        .getDeclaredMethod("onCreateNavController", NavController::class.java)

                    onCreateNavController.isAccessible = true
                    onCreateNavController.invoke(uut, mockNavController)
                }

                it("adds an HttpNavigator to the navigator provider") {
                    verify(uut?.navigatorFactory)?.newHttpNavigatorInstance(
                        alertDialogBuilderFactory = uut?.alertDialogBuilderFactory!!,
                        context = uut?.activity!!,
                        intentFactory = uut?.intentFactory!!,
                        uriForwarder = uut?.uriForwarder!!
                    )

                    verify(mockNavigatorProvider)?.addNavigator(mockHttpNavigator!!)
                }
            }

            describe("and the host fragment does not have a reference to the containing activity") {

                beforeEachTest {
                    mockNavigatorProvider = mock()

                    val mockContext: Context = mock()
                    val mockFragmentManger: FragmentManager = mock()
                    val mockNavController: NavController = mock {
                        on { navigatorProvider } doReturn mockNavigatorProvider!!
                    }

                    doReturn(mockContext).whenever(uut)?.context
                    doReturn(mockFragmentManger).whenever(uut)?.childFragmentManager
                    doReturn(null).whenever(uut)?.activity

                    val onCreateNavController = CustomNavHostFragment::class.java
                        .getDeclaredMethod("onCreateNavController", NavController::class.java)

                    onCreateNavController.isAccessible = true
                    onCreateNavController.invoke(uut, mockNavController)
                }

                it("does not add an HttpNavigator to the navigator provider") {
                    verify(uut?.navigatorFactory, never())?.newHttpNavigatorInstance(
                        alertDialogBuilderFactory = any(),
                        context = any(),
                        intentFactory = any(),
                        uriForwarder = any()
                    )

                    verify(mockNavigatorProvider, never())?.addNavigator(any<HttpNavigator>())
                }
            }
        }
    }
})
