package com.oliverspryn.blog.androidunittests.navigation.navigators

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.content.res.TypedArray
import android.net.Uri
import android.os.Bundle
import android.util.AttributeSet
import androidx.appcompat.app.AlertDialog
import androidx.navigation.NavDestination
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.capture
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.R
import com.oliverspryn.blog.androidunittests.wrappers.android.AlertDialogBuilderFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.IntentFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.UriForwarder
import com.winterbe.expekt.expect
import org.mockito.ArgumentCaptor
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class HttpNavigatorTest : Spek({

    describe("The HttpNavigator") {

        var mockAlertDialogBuilderFactory: AlertDialogBuilderFactory? = null
        var mockContext: Context? = null
        var mockIntentFactory: IntentFactory? = null
        var mockUriForwarder: UriForwarder? = null

        var uut: HttpNavigator? = null

        beforeEachTest {

            mockAlertDialogBuilderFactory = mock()
            mockContext = mock()
            mockIntentFactory = mock()
            mockUriForwarder = mock()

            uut = HttpNavigator(
                alertDialogBuilderFactory = mockAlertDialogBuilderFactory!!,
                context = mockContext!!,
                intentFactory = mockIntentFactory!!,
                uriForwarder = mockUriForwarder!!
            )
        }

        describe("when createDestination") {

            var result: HttpNavigator.Destination? = null

            beforeEachTest {
                result = uut?.createDestination()
            }

            it("returns an HttpNavigator destination") {
                expect(result).to.be.instanceof(HttpNavigator.Destination::class.java)
                expect(result).not.to.be.`null`
            }
        }

        describe("when navigate") {

            var mockAlertDialog: AlertDialog? = null
            var mockAlertDialogBuilder: AlertDialog.Builder? = null
            var mockArgs: Bundle? = null
            var mockDestination: HttpNavigator.Destination? = null
            var mockIntent: Intent? = null
            var mockUri: Uri? = null

            var result: NavDestination? = null

            beforeEachTest {

                mockAlertDialog = mock()
                mockAlertDialogBuilder = mock()
                mockArgs = mock()
                mockDestination = mock()
                mockIntent = mock()
                mockUri = mock()

                whenever(mockAlertDialogBuilder?.create()).thenReturn(mockAlertDialog)
                whenever(mockAlertDialogBuilderFactory?.newInstance(mockContext!!)).thenReturn(mockAlertDialogBuilder)
            }

            describe("and the destination has an empty URL property and the device is able to open the link") {

                beforeEachTest {
                    whenever(mockArgs?.getParcelable<Uri>("uri")).thenReturn(mockUri)
                    whenever(mockDestination?.url).thenReturn("")
                    whenever(mockIntentFactory?.newInstance(Intent.ACTION_VIEW, mockUri!!)).thenReturn(mockIntent)

                    result = uut?.navigate(
                        destination = mockDestination!!,
                        args = mockArgs!!,
                        navOptions = null,
                        navigatorExtras = null
                    )
                }

                it("navigates to default HTTP handler for the given link") {
                    verify(mockContext)?.startActivity(mockIntent!!)
                }

                it("returns null") {
                    expect(result).to.be.`null`
                }
            }

            describe("and the destination has an empty URL property and the device is not able to open the link") {

                var clickArgumentCaptor: ArgumentCaptor<DialogInterface.OnClickListener>? = null

                beforeEachTest {
                    clickArgumentCaptor = ArgumentCaptor.forClass(DialogInterface.OnClickListener::class.java)

                    whenever(mockAlertDialogBuilder?.setPositiveButton(eq(R.string.ok), capture(clickArgumentCaptor!!))).thenReturn(mockAlertDialogBuilder)
                    whenever(mockArgs?.getParcelable<Uri>("uri")).thenReturn(mockUri)
                    whenever(mockDestination?.url).thenReturn("")
                    whenever(mockIntentFactory?.newInstance(Intent.ACTION_VIEW, mockUri!!)).thenReturn(mockIntent)

                    whenever(mockContext?.startActivity(mockIntent)).thenAnswer {
                        throw ActivityNotFoundException()
                    }

                    uut?.navigate(
                        destination = mockDestination!!,
                        args = mockArgs!!,
                        navOptions = null,
                        navigatorExtras = null
                    )
                }

                it("creates and shows a dialog with an error message and an OK button") {
                    verify(mockAlertDialogBuilder)?.setCancelable(true)
                    verify(mockAlertDialogBuilder)?.setMessage(R.string.browser_unavailable)
                    verify(mockAlertDialogBuilder)?.setPositiveButton(eq(R.string.ok), any())
                    verify(mockAlertDialogBuilder)?.create()
                    verify(mockAlertDialog)?.show()
                }

                describe("that OK button's click listener") {

                    var mockDialogInterface: DialogInterface? = null

                    beforeEachTest {
                        mockDialogInterface = mock()
                        clickArgumentCaptor?.value?.onClick(mockDialogInterface, 42)
                    }

                    it("closes the dialog") {
                        verify(mockDialogInterface)?.cancel()
                    }
                }

                it("returns null") {
                    expect(result).to.be.`null`
                }
            }

            describe("and the destination has a URL property and the device is able to open the link") {

                beforeEachTest {
                    whenever(mockDestination?.url).thenReturn("https://duckduckgo.com/")
                    whenever(mockIntentFactory?.newInstance(Intent.ACTION_VIEW, mockUri!!)).thenReturn(mockIntent)
                    whenever(mockUriForwarder?.parse("https://duckduckgo.com/")).thenReturn(mockUri)

                    result = uut?.navigate(
                        destination = mockDestination!!,
                        args = mockArgs!!,
                        navOptions = null,
                        navigatorExtras = null
                    )
                }

                it("navigates to default HTTP handler for the given link") {
                    verify(mockContext)?.startActivity(mockIntent!!)
                }

                it("returns null") {
                    expect(result).to.be.`null`
                }
            }

            describe("and the destination has a URL property and the device is not able to open the link") {

                var clickArgumentCaptor: ArgumentCaptor<DialogInterface.OnClickListener>? = null

                beforeEachTest {
                    clickArgumentCaptor = ArgumentCaptor.forClass(DialogInterface.OnClickListener::class.java)

                    whenever(mockAlertDialogBuilder?.setPositiveButton(eq(R.string.ok), capture(clickArgumentCaptor!!))).thenReturn(mockAlertDialogBuilder)
                    whenever(mockDestination?.url).thenReturn("https://duckduckgo.com/")
                    whenever(mockIntentFactory?.newInstance(Intent.ACTION_VIEW, mockUri!!)).thenReturn(mockIntent)
                    whenever(mockUriForwarder?.parse("https://duckduckgo.com/")).thenReturn(mockUri)

                    whenever(mockContext?.startActivity(mockIntent)).thenAnswer {
                        throw ActivityNotFoundException()
                    }

                    uut?.navigate(
                        destination = mockDestination!!,
                        args = mockArgs!!,
                        navOptions = null,
                        navigatorExtras = null
                    )
                }

                it("creates and shows a dialog with an error message and an OK button") {
                    verify(mockAlertDialogBuilder)?.setCancelable(true)
                    verify(mockAlertDialogBuilder)?.setMessage(R.string.browser_unavailable)
                    verify(mockAlertDialogBuilder)?.setPositiveButton(eq(R.string.ok), any())
                    verify(mockAlertDialogBuilder)?.create()
                    verify(mockAlertDialog)?.show()
                }

                describe("that OK button's click listener") {

                    var mockDialogInterface: DialogInterface? = null

                    beforeEachTest {
                        mockDialogInterface = mock()
                        clickArgumentCaptor?.value?.onClick(mockDialogInterface, 42)
                    }

                    it("closes the dialog") {
                        verify(mockDialogInterface)?.cancel()
                    }
                }

                it("returns null") {
                    expect(result).to.be.`null`
                }
            }

            describe("and no arguments were provided and the destination does not have a URL property") {

                beforeEachTest {
                    whenever(mockDestination?.url).thenReturn("")

                    result = uut?.navigate(
                        destination = mockDestination!!,
                        args = null,
                        navOptions = null,
                        navigatorExtras = null
                    )
                }

                it("does not navigate to any intent") {
                    verify(mockContext, never())?.startActivity(any())
                }

                it("does not create and show any error dialog") {
                    verify(mockAlertDialogBuilder, never())?.create()
                    verify(mockAlertDialog, never())?.show()
                }

                it("returns null") {
                    expect(result).to.be.`null`
                }
            }
        }

        describe("when popBackStack") {

            var result: Boolean? = null

            beforeEachTest {
                result = uut?.popBackStack()
            }

            it("returns true") {
                expect(result).to.be.`true`
            }
        }

        describe("the Destination") {

            var uutDestination: HttpNavigator.Destination? = null

            beforeEachTest {
                uutDestination = HttpNavigator.Destination(
                    navigator = uut!!
                )
            }

            describe("when init") {
                it("the cached URL is empty") {
                    expect(uutDestination?.url).to.be.empty
                }
            }

            describe("when onInflate") {

                var mockAttributeSet: AttributeSet? = null
                var mockTypedArray: TypedArray? = null

                beforeEachTest {

                    mockTypedArray = mock {
                        on { getResourceId(any(), any()) } doReturn 42
                    }

                    val mockResources: Resources = mock {
                        on { obtainAttributes(any(), any()) } doReturn mockTypedArray
                    }

                    mockAttributeSet = mock()

                    whenever(mockContext?.resources).thenReturn(mockResources)
                    doReturn(mockTypedArray).whenever(mockContext)?.obtainStyledAttributes(
                        mockAttributeSet,
                        R.styleable.HttpNavigator,
                        0,
                        0
                    )
                }

                describe("and the string property from the XML can be obtained") {

                    beforeEachTest {
                        whenever(mockTypedArray?.getString(R.styleable.HttpNavigator_url))
                            .thenReturn("https://duckduckgo.com/")

                        uutDestination?.onInflate(mockContext!!, mockAttributeSet!!)
                    }

                    it("sets the URL") {
                        expect(uutDestination?.url).to.equal("https://duckduckgo.com/")
                    }
                }

                describe("and the string property from the XML cannot be obtained") {

                    beforeEachTest {
                        whenever(mockTypedArray?.getString(R.styleable.HttpNavigator_url))
                            .thenReturn(null)

                        uutDestination?.url = "https://duckduckgo.com/"
                        uutDestination?.onInflate(mockContext!!, mockAttributeSet!!)
                    }

                    it("sets a blank URL") {
                        expect(uutDestination?.url).to.equal("")
                    }
                }
            }
        }
    }
})
