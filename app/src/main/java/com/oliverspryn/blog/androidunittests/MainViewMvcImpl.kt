package com.oliverspryn.blog.androidunittests

import android.view.LayoutInflater
import android.view.ViewGroup
import com.oliverspryn.blog.androidunittests.MainViewMvc
import com.oliverspryn.blog.androidunittests.R
import com.oliverspryn.blog.androidunittests.mvc.BaseViewMvc

class MainViewMvcImpl(
    inflater: LayoutInflater,
    parent: ViewGroup?
) : BaseViewMvc(), MainViewMvc {

    init {
        rootView = inflater.inflate(R.layout.activity_main, parent, false)
    }
}
