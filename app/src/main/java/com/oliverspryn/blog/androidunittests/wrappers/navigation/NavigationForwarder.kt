package com.oliverspryn.blog.androidunittests.wrappers.navigation

import android.view.View
import androidx.navigation.Navigation
import javax.inject.Inject

class NavigationForwarder @Inject constructor() {
    fun findNavController(view: View) = Navigation.findNavController(view)
}
