package com.oliverspryn.blog.androidunittests.navigation

import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.oliverspryn.blog.androidunittests.dagger.DaggerInjector
import com.oliverspryn.blog.androidunittests.dagger.components.MainComponent
import com.oliverspryn.blog.androidunittests.navigation.navigators.HttpNavigator
import com.oliverspryn.blog.androidunittests.wrappers.android.AlertDialogBuilderFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.IntentFactory
import com.oliverspryn.blog.androidunittests.wrappers.android.UriForwarder
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavigatorFactory
import javax.inject.Inject

class CustomNavHostFragment(
    mainComponent: MainComponent? = DaggerInjector.mainComponent
) : NavHostFragment() {

    @Inject
    lateinit var alertDialogBuilderFactory: AlertDialogBuilderFactory

    @Inject
    lateinit var intentFactory: IntentFactory

    @Inject
    lateinit var navigatorFactory: NavigatorFactory

    @Inject
    lateinit var uriForwarder: UriForwarder

    init {
        mainComponent?.inject(this)
    }

    override fun onCreateNavController(navController: NavController) {
        super.onCreateNavController(navController)

        activity?.let {
            navController.navigatorProvider.addNavigator(
                navigatorFactory.newHttpNavigatorInstance(
                    alertDialogBuilderFactory = alertDialogBuilderFactory,
                    context = it,
                    intentFactory = intentFactory,
                    uriForwarder = uriForwarder
                )
            )
        }
    }
}
