package com.oliverspryn.blog.androidunittests.details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.oliverspryn.blog.androidunittests.dagger.DaggerInjector
import com.oliverspryn.blog.androidunittests.dagger.components.MainComponent
import com.oliverspryn.blog.androidunittests.mvc.ViewMvcFactory
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavArgsForwarder
import com.oliverspryn.blog.androidunittests.wrappers.PicassoForwarder
import javax.inject.Inject

class DetailsFragment(
    mainComponent: MainComponent? = DaggerInjector.mainComponent
) : Fragment() {

    @Inject
    lateinit var detailsController: DetailsController

    @Inject
    lateinit var navArgsForwarder: NavArgsForwarder

    @Inject
    lateinit var picassoForwarder: PicassoForwarder

    @Inject
    lateinit var viewMvcFactory: ViewMvcFactory

    init {
        mainComponent?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val viewMvc = viewMvcFactory.getDetailsViewMvc(container, picassoForwarder)
        detailsController.viewMvc = viewMvc

        arguments?.let {
            val deseralizedArgs = navArgsForwarder.detailsFragmentArgs(it)
            detailsController.onCreateView(deseralizedArgs.photo)
        }

        return viewMvc.rootView
    }

    override fun onStart() {
        super.onStart()
        detailsController.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        detailsController.onDestroy()
    }
}
