package com.oliverspryn.blog.androidunittests.wrappers.android

import android.content.Context
import androidx.appcompat.app.AlertDialog

class AlertDialogBuilderFactory {
    fun newInstance(context: Context) = AlertDialog.Builder(context)
}
