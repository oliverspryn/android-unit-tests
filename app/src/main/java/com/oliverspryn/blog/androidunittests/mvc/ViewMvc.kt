package com.oliverspryn.blog.androidunittests.mvc

import android.view.View

interface ViewMvc {
    val rootView: View
}
