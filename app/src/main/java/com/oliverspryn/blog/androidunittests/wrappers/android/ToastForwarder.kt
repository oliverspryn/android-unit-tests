package com.oliverspryn.blog.androidunittests.wrappers.android

import android.content.Context
import android.widget.Toast

class ToastForwarder {
    fun makeText(
        context: Context,
        text: CharSequence,
        duration: Int
    ): Toast? = Toast.makeText(
        context,
        text,
        duration
    )
}
