package com.oliverspryn.blog.androidunittests.wrappers.navigation

import android.net.Uri
import com.oliverspryn.blog.androidunittests.details.DetailsFragmentDirections
import com.oliverspryn.blog.androidunittests.photos.PhotosFragmentDirections
import com.oliverspryn.blog.androidunittests.photos.PhotosModel
import javax.inject.Inject

class NavDirectionsForwarder @Inject constructor() {
    fun actionDetailsFragmentToPreviewPictureUrl(
        uri: Uri
    ) = DetailsFragmentDirections.actionDetailsFragmentToPreviewPictureUrl(
        uri
    )

    fun actionPhotoListFragmentToDetailsFragment(
        photo: PhotosModel
    ) = PhotosFragmentDirections.actionPhotoListFragmentToDetailsFragment(
        photo
    )
}
