package com.oliverspryn.blog.androidunittests.mvc

import android.view.LayoutInflater
import android.view.ViewGroup
import com.oliverspryn.blog.androidunittests.MainViewMvc
import com.oliverspryn.blog.androidunittests.MainViewMvcImpl
import com.oliverspryn.blog.androidunittests.details.DetailsViewMvc
import com.oliverspryn.blog.androidunittests.details.DetailsViewMvcImpl
import com.oliverspryn.blog.androidunittests.photos.PhotosViewMvc
import com.oliverspryn.blog.androidunittests.photos.PhotosViewMvcImpl
import com.oliverspryn.blog.androidunittests.photos.photolistitem.PhotoListItemViewMvc
import com.oliverspryn.blog.androidunittests.photos.photolistitem.PhotoListItemViewMvcImpl
import com.oliverspryn.blog.androidunittests.wrappers.android.LinearLayoutManagerFactory
import com.oliverspryn.blog.androidunittests.wrappers.PicassoForwarder
import javax.inject.Inject

class ViewMvcFactory @Inject constructor(
    private val layoutInflater: LayoutInflater
) {

    fun getDetailsViewMvc(
        parent: ViewGroup?,
        picassoForwarder: PicassoForwarder
    ): DetailsViewMvc = DetailsViewMvcImpl(
        inflater = layoutInflater,
        parent = parent,
        picassoForwarder = picassoForwarder
    )

    fun getMainViewMvc(
        parent: ViewGroup?,
    ): MainViewMvc = MainViewMvcImpl(
        inflater = layoutInflater,
        parent = parent
    )

    fun getPhotoListItemViewMvc(
        parent: ViewGroup?,
        picassoForwarder: PicassoForwarder
    ): PhotoListItemViewMvc = PhotoListItemViewMvcImpl(
        inflater = layoutInflater,
        parent = parent,
        picassoForwarder = picassoForwarder
    )

    fun getPhotosViewMvc(
        linearLayoutManagerFactory: LinearLayoutManagerFactory,
        parent: ViewGroup?
    ): PhotosViewMvc = PhotosViewMvcImpl(
        inflater = layoutInflater,
        linearLayoutManagerFactory = linearLayoutManagerFactory,
        parent = parent
    )
}
