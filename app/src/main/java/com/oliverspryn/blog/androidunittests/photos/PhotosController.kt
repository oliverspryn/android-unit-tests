package com.oliverspryn.blog.androidunittests.photos

import android.widget.Toast
import androidx.annotation.VisibleForTesting
import com.oliverspryn.blog.androidunittests.R
import com.oliverspryn.blog.androidunittests.photos.photolistitem.PhotoListItemViewMvc
import com.oliverspryn.blog.androidunittests.mvc.ViewMvcFactory
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavDirectionsForwarder
import com.oliverspryn.blog.androidunittests.wrappers.navigation.NavigationForwarder
import com.oliverspryn.blog.androidunittests.wrappers.PhotosFactory
import com.oliverspryn.blog.androidunittests.wrappers.PicassoForwarder
import com.oliverspryn.blog.androidunittests.wrappers.SchedulerForwarder
import com.oliverspryn.blog.androidunittests.wrappers.android.ToastForwarder
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PhotosController @Inject constructor(
    private val compositeDisposable: CompositeDisposable,
    private val navDirectionsForwarder: NavDirectionsForwarder,
    private val navigationForwarder: NavigationForwarder,
    private val photosApiService: PhotosApiService,
    private val photosFactory: PhotosFactory,
    private val picassoForwarder: PicassoForwarder,
    private val schedulerForwarder: SchedulerForwarder,
    private val toastForwarder: ToastForwarder,
    private val viewMvcFactory: ViewMvcFactory
) : PhotoListItemViewMvc.Listener {

    @VisibleForTesting
    var photosAdapter: PhotosAdapter? = null

    var viewMvc: PhotosViewMvc? = null

    fun onStart() {
        val adapter = photosFactory.newAdapterInstance(
            listener = this,
            picassoForwarder = picassoForwarder,
            viewMvcFactory = viewMvcFactory
        )

        viewMvc?.setAdapter(adapter)
        getPhotos()

        photosAdapter = adapter
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

    // region PhotoListItemViewMvc

    override fun onPhotoTap(photoModel: PhotosModel) {
        viewMvc?.rootView?.let {
            val directions = navDirectionsForwarder.actionPhotoListFragmentToDetailsFragment(photoModel)

            navigationForwarder
                .findNavController(it)
                .navigate(directions)
        }
    }

    // endregion

    @VisibleForTesting
    fun getPhotos() {
        photosApiService
            .getPhotos()
            .subscribeOn(schedulerForwarder.io)
            .observeOn(schedulerForwarder.mainThread)
            .subscribe({ photos ->
                photosAdapter?.updatePhotos(photos)
            }, {
                viewMvc?.rootView?.context?.let { context ->
                    val message = context.getString(R.string.network_error)

                    toastForwarder.makeText(
                        context,
                        message,
                        Toast.LENGTH_LONG
                    )?.show()
                }
            })
            .let {
                compositeDisposable.add(it)
            }
    }
}
