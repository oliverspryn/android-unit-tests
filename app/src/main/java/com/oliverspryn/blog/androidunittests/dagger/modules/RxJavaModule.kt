package com.oliverspryn.blog.androidunittests.dagger.modules

import com.oliverspryn.blog.androidunittests.wrappers.SchedulerForwarder
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class RxJavaModule {

    @Provides
    fun provideCompositeDisposable() = CompositeDisposable()

    @Provides
    fun provideSchedulerForwarder() = SchedulerForwarder()
}
