# Android Unit Tests

Companion code for my blog article: Writing a Fully Unit Testable Android App

## Running the Application

To build this application, simply run:

```bash
./gradlew assembleDebug
```

The output is shown in `app/build/outputs/apk/debug/app-debug.apk`

Running unit tests and extracting the coverage report is also just as easy:

```bash
./gradlew testDebug extractTestReport
```

The coverage reports appear in `app/build/reports/jacoco/jacocoTestReport`
